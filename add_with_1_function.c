//Write a program to add two user input numbers using one function.
#include<stdio.h>

float sum1(float a, float b)
{
  return (a+b);
}

int main()
{
  float a, b;
  printf("Enter two numbers: ");
  scanf("%f%f",&a,&b);
  printf("Sum of %f and %f is: %f\n",a,b,sum1(a,b));
  return 0;
}

