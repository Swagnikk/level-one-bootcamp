//WAP to find the sum of two fractions.
#include<stdio.h>

struct fraction
{
 	int num1,denom1,num2,denom2;
};

int lcm(struct fraction d1, struct fraction d2)
{
 	int max=0;
 	if(d1.denom1>d2.denom2)
 	{
      	max=d1.denom1;
 	}
 	else
 	{
      	max=d2.denom2;
 	}
 	while(1)
 	{
      	if (max%d1.denom1 == 0 && max%d2.denom2 == 0)
      	{
          	break;
      	}
      	max++;
 	}  
return max;
}

int num1(struct fraction n1, struct fraction d1, struct fraction d2)
{
  	n1.num1 *= lcm(d1,d2)/d1.denom1;
}

int num2(struct fraction n2, struct fraction d1, struct fraction d2)
{
  	n2.num2 *= lcm(d1,d2)/d2.denom2;
}

int output(struct fraction n1, struct fraction d1, struct fraction n2, struct fraction d2)
{
 	return num1(n1,d1,d2) + num2(n2,d1,d2);
}

int gcd(int num, int denom)
{
    int ans;
    while(num % denom != 0)
    {
        ans = num % denom;
        num = denom;
        denom = ans;
    }
    return denom;
}

void result(struct fraction n1, struct fraction d1, struct fraction n2, struct fraction d2)
{
    int num = output(n1,d1,n2,d2);
 	int denom = lcm(d1,d2);
    printf("Sum of %d/%d and %d/%d is: \n", n1.num1, d1.denom1, n2.num2, d2.denom2);
 	printf("%d/%d",(num1(n1,d1,d2) + num2(n2,d1,d2))/gcd(num,denom), lcm(d1,d2)/gcd(num,denom));
}


int main()
{
 	struct fraction n1,d1,n2,d2;
 	printf("FOR FIRST FRACTION: \n");
 	printf("Numerator 1:  ");
 	scanf("%d",&n1.num1);
 	printf("Denominator 1: ");
 	scanf("%d",&d1.denom1);
 	printf("FOR SECOND FRACTION: \n");
 	printf("Numerator 2: ");
 	scanf("%d",&n2.num2);
 	printf("Denominator 2: ");
 	scanf("%d",&d2.denom2);
	 
    result(n1,d1,n2,d2);
 	return 0;
}


