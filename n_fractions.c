//WAP to find the sum of n fractions.
#include<stdio.h>

struct frac{
   int num, den;
};
typedef struct frac fraction;

fraction input()
{
   fraction f;
   printf("Enter Numerator and Denominator: \n");
   scanf("%d%d",&f.num,&f.den);
   return f;
}

fraction add(fraction f1, fraction f2)
{
   fraction ans;
   ans.num=(f1.num*f2.den)+(f2.num*f1.den);
   ans.den=f1.den*f2.den;
   return ans;
}

int gcd(int num, int den)
{
   if(num==0)
   {
      return den;
   }
   return gcd(den%num,num);
}

fraction lowestTerm(fraction f)
{
   int value=gcd(f.num,f.den);
   f.num/=value;
   f.den/=value;
   return f;
}

fraction compute_total(fraction arr[ ], int n)
{
   fraction total;
   total.num=0;
   total.den=1;
   for(int i=0; i<n; i++)
   {
      total=add(total, arr[i]);
   }
   total=lowestTerm(total);
   return total;
}

void output(fraction ans)
{
   printf("Sum of all fractions: %d/%d\n",ans.num,ans.den);
}

int main()
{
   fraction arr[50],ans;
   int n;
   printf("No. of fractions: \n");
   scanf("%d",&n);

   if (n==1)
   {
      output(input());
      return 0;
   }
   for (int i=0; i<n; i++)
   {
      arr[i]=input();
   }
   ans=compute_total(arr,n);
   output(ans);
   return 0;
}
